package com.accenture.reboarding.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class ReBoardServiceImplementationTest {

    private ReBoardServiceImplementation reBoardServiceImplementation;

    @BeforeEach
    void setUp() {
        this.reBoardServiceImplementation = new ReBoardServiceImplementation();
        this.reBoardServiceImplementation.setCapacity(100);
        this.reBoardServiceImplementation.setCapacityPercentage(2);
        this.reBoardServiceImplementation.register("employee17");
    }

    @Test
    void testRegisterWithNewId() {
        String result = this.reBoardServiceImplementation.register("employee18");
        assertEquals("You are in the registered to enter list.", result);
    }

    @Test
    void testRegisterWithNewIdSizeCheck() {
        this.reBoardServiceImplementation.register("employee18");
        assertEquals(2, this.reBoardServiceImplementation.getEmployeeIdAccessGrantedListSize());
    }

    @Test
    void testRegisterWithSameId() {
        String result = this.reBoardServiceImplementation.register("employee17");
        assertEquals("Already registered: You are in the registered to enter list. You can go to work", result);
    }

    @Test
    void testRegisterWithSameIdSizeCheck() {
        this.reBoardServiceImplementation.register("employee17");
        assertEquals(1, this.reBoardServiceImplementation.getEmployeeIdAccessGrantedListSize());
    }

    @Test
    void testRegisterWithMoreThanAllowedEmployeeCapacity() {
        this.reBoardServiceImplementation.register("employee18");
        String result = this.reBoardServiceImplementation.register("employee19");
        assertEquals("1", result);
    }

    @Test
    void testRegisterWithMoreThanAllowedEmployeeCapacitySizeCheck() {
        this.reBoardServiceImplementation.register("employee18");
        this.reBoardServiceImplementation.register("employee19");
        assertEquals(1, this.reBoardServiceImplementation.getEmployeeIdWaitingListSize());
    }

    @Test
    void testRegisterAlreadyRegisteredToWaitingList() {
        this.reBoardServiceImplementation.register("employee18");
        this.reBoardServiceImplementation.register("employee19");
        String result = this.reBoardServiceImplementation.register("employee19");
        assertEquals("Already registered: Your position in the waiting list: 1", result);
    }

    @Test
    void testRegisterAlreadyRegisteredToWaitingListSizeCheck() {
        this.reBoardServiceImplementation.register("employee18");
        this.reBoardServiceImplementation.register("employee19");
        this.reBoardServiceImplementation.register("employee19");
        assertEquals(1, this.reBoardServiceImplementation.getEmployeeIdWaitingListSize());
    }

    @Test
    void testStatusRegisteredToAccessGrantedList() {
        String result = this.reBoardServiceImplementation.status("employee17");
        assertEquals("You are in the registered to enter list. You can go to work", result);
    }

    @Test
    void testStatusRegisteredEmployeeToWaitingList() {
        this.reBoardServiceImplementation.register("employee18");
        this.reBoardServiceImplementation.register("employee19");
        String result = this.reBoardServiceImplementation.status("employee19");
        assertEquals("Your position in the waiting list: 1", result);
    }

    @Test
    void testStatusRegisteredSecondEmployeeToWaitingList() {
        this.reBoardServiceImplementation.register("employee18");
        this.reBoardServiceImplementation.register("employee19");
        this.reBoardServiceImplementation.register("employee20");
        String result = this.reBoardServiceImplementation.status("employee20");
        assertEquals("Your position in the waiting list: 2", result);
    }

    @Test
    void testEntryWithRegisteredToAccessGrantedList() {
        assertTrue(this.reBoardServiceImplementation.entry("employee17"));
    }

    @Test
    void testEntryWithoutRegistration() {
        assertFalse(this.reBoardServiceImplementation.entry("employee23"));
    }

    @Test
    void testEntryRegisteredToWaitingList() {
        this.reBoardServiceImplementation.register("employee18");
        this.reBoardServiceImplementation.register("employee19");
        assertFalse(this.reBoardServiceImplementation.entry("employee19"));
    }

    @Test
    void testExitRegisteredToAccessGrantedList() {
        this.reBoardServiceImplementation.exit("employee17");
        assertEquals(0, this.reBoardServiceImplementation.getEmployeeIdAccessGrantedListSize());
    }

    @Test
    void testExitRegisteredToAccessGrantedListAndForUpdatingRegisteredDatabase() {
        this.reBoardServiceImplementation.register("employee18");
        this.reBoardServiceImplementation.register("employee19");
        this.reBoardServiceImplementation.exit("employee17");
        assertEquals(0, this.reBoardServiceImplementation.getEmployeeIdWaitingListSize());
    }
}