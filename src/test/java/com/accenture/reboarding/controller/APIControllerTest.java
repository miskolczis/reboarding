package com.accenture.reboarding.controller;

import com.accenture.reboarding.service.ReBoardService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
public class APIControllerTest {

    private String body = "employee17";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReBoardService mockedReBoardService;

    @Test
    public void testRegister() throws Exception {

        String URI = "/register";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(URI)
                .param("employeeId", this.body);

        Mockito.when(mockedReBoardService.register(Mockito.any(String.class))).thenReturn("ok");
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        String outputString = response.getContentAsString();
        Assertions.assertEquals("ok", outputString);
        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void testStatus() throws Exception {

        String URI = "/status";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(URI)
                .param("employeeId", this.body);

        Mockito.when(mockedReBoardService.status(Mockito.any(String.class))).thenReturn("ok");
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        String outputString = response.getContentAsString();
        Assertions.assertEquals("ok", outputString);
        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void testEntry() throws Exception {

        String URI = "/entry";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(URI)
                .param("employeeId", this.body);

        Mockito.when(mockedReBoardService.entry(Mockito.any(String.class))).thenReturn(true);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        String outputString = response.getContentAsString();
        Assertions.assertEquals("true", outputString);
        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void testExit() throws Exception {

        String URI = "/exit";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(URI)
                .param("employeeId", this.body);

        Mockito.when(mockedReBoardService.exit(Mockito.any(String.class))).thenReturn(true);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        String outputString = response.getContentAsString();
        Assertions.assertEquals("true", outputString);
        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
    }
}

