package com.accenture.reboarding.service;

public interface ReBoardService {

    String register(String employeeId);

    String status(String employeeId);

    boolean entry(String employeeId);

    boolean exit(String employeeId);
}
