package com.accenture.reboarding.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

@Service
public class ReBoardServiceImplementation implements ReBoardService {

    private static final Logger log = LoggerFactory.getLogger(ReBoardServiceImplementation.class);

    private final Set<String> employeeIdAccessGrantedList = new HashSet<>();
    private final Set<String> employeeIdWaitingList = new LinkedHashSet<>();
    @Value("${com.accenture.reboard.service.capacity}")
    private int capacity;
    @Value("${com.accenture.reboard.service.capacityPercentage}")
    private int capacityPercentage;

    /**
     * This method allows user to register in 2 types of lists. Employees could be registered to a list with
     * employees access granted or a waiting list entering employee id.
     *
     * @param employeeId
     * @return Method returns whether the employee already registered in a list access granted
     * or in waiting list. When employee not registered in waiting list, method returns also the position
     * in the waiting list.
     */
    @Override
    public String register(String employeeId) {
        String positionInWaitingList;
        if (this.employeeIdAccessGrantedList.contains(employeeId) || this.employeeIdWaitingList.contains(employeeId)) {
            positionInWaitingList = "Already registered: " + status(employeeId);
        } else {
            if (this.employeeIdAccessGrantedList.size() < this.capacity * this.capacityPercentage / 100) {
                this.employeeIdAccessGrantedList.add(employeeId);
                positionInWaitingList = "You are in the registered to enter list.";
            } else {
                this.employeeIdWaitingList.add(employeeId);
                positionInWaitingList = "" + (employeeIdWaitingList.size());
            }
        }
        return positionInWaitingList;
    }

    /**
     * Check for employee status by employee id.
     *
     * @param employeeId
     * @return When access granted for employee, method returns message else employee in the waiting list,
     * method returns position of employee in waiting list. When employee not registered, employee receives
     * suggestion to register first.
     */
    @Override
    public String status(String employeeId) {
        String positionInWaitingList = "You not registered. Register first!";
        if (employeeIdAccessGrantedList.contains(employeeId)) {
            positionInWaitingList = "You are in the registered to enter list. You can go to work";
        } else if (employeeIdWaitingList.size() > 0 && this.employeeIdWaitingList.contains(employeeId)) {
            int position = 0;
            String actualEmployeeID;
            Iterator<String> iterator = employeeIdWaitingList.iterator();
            do {
                actualEmployeeID = iterator.next();
                position++;
            } while (iterator.hasNext() && !actualEmployeeID.equals(employeeId));
            positionInWaitingList = "Your position in the waiting list: " + position;
        }
        return positionInWaitingList;
    }

    /**
     * Method checks if employee with scanned employee id could enter.
     *
     * @param employeeId
     * @return Returns a boolean which represents the result of the check.
     */
    @Override
    public boolean entry(String employeeId) {
        log.info(String.format("Employee entered: %s %d", employeeId, System.currentTimeMillis()));
        return employeeIdAccessGrantedList.contains(employeeId);
    }

    /**
     * Method updates the registrations in both lists.
     *
     * @param employeeId
     */
    @Override
    public boolean exit(String employeeId) {
        if (employeeIdAccessGrantedList.remove(employeeId) && employeeIdWaitingList.size() > 0) {
            Iterator<String> iterator = employeeIdWaitingList.iterator();
            String firstInWaitingEmployeeID = iterator.next();
            iterator.remove();
            employeeIdAccessGrantedList.add(firstInWaitingEmployeeID);
            log.info(String.format("Exited employee: %s %d", employeeId, System.currentTimeMillis()));
            return true;
        } else {
            log.error(String.format("Not registered employee exited: %s %d", employeeId, System.currentTimeMillis()));
            return false;
        }
    }

    void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    void setCapacityPercentage(int capacityPercentage) {
        this.capacityPercentage = capacityPercentage;
    }

    int getEmployeeIdAccessGrantedListSize() {
        return this.employeeIdAccessGrantedList.size();
    }

    int getEmployeeIdWaitingListSize() {
        return this.employeeIdWaitingList.size();
    }
}
