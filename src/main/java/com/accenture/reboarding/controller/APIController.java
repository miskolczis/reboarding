package com.accenture.reboarding.controller;

import com.accenture.reboarding.service.ReBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class APIController {

    private ReBoardService reBoardService;

    @Autowired
    public void setReBoardService(ReBoardService reBoardService) {
        this.reBoardService = reBoardService;
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public String register(@RequestParam(value = "employeeId", required = true) String employeeId) {
        return this.reBoardService.register(employeeId);
    }

    @RequestMapping(path = "/status", method = RequestMethod.POST)
    public String status(@RequestParam(value = "employeeId", required = true) String employeeId) {
        return this.reBoardService.status(employeeId);
    }

    @RequestMapping(path = "/entry", method = RequestMethod.POST)
    public boolean entry(@RequestParam(value = "employeeId", required = true) String employeeId) {
        return this.reBoardService.entry(employeeId);
    }

    @RequestMapping(path = "/exit", method = RequestMethod.POST)
    public boolean exit(@RequestParam(value = "employeeId", required = true) String employeeId) {
        return this.reBoardService.exit(employeeId);
    }
}
