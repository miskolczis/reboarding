package com.accenture.reboarding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReBoardingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReBoardingApplication.class, args);
    }

}
